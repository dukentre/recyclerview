package ru.dukentre.recycleview

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Range
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val array = ArrayList<FilmItem>();
        val words = ArrayList<String>();
        words.add("Dukentre ")
        words.add("был ")
        words.add("здесь ")
        words.add("Герасимов ")
        words.add("Android ")
        words.add("Is ")
        words.add("Shit ")

        for(i in 0..100){
            var title = "";
            var description = "";
            for(a in 0..3){
                    title+=words[Random.nextInt(0,words.size)];
            }
            for(a in 0..8){
                description+=words[Random.nextInt(0,words.size)];
            }
            array.add(FilmItem(title,description,ContextCompat.getDrawable(this,R.drawable.snus_babysya)))
        }


        val recyclerView = findViewById<RecyclerView>(R.id.RecyclerFilms);

        recyclerView.layoutManager = LinearLayoutManager(this);
        recyclerView.adapter = FilmsAdapter(array);
    }
}

class FilmItem(val Title:String, val Description:String, val Image:Drawable?)

class FilmsAdapter(val list:ArrayList<FilmItem>):RecyclerView.Adapter<FilmsViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_films,parent,false);
        return FilmsViewHolder(view);
    }

    override fun getItemCount(): Int {
        return list.size;
    }

    override fun onBindViewHolder(holder: FilmsViewHolder, position: Int) {
        val filmItem = list[position];
        holder.titleView.text = filmItem.Title;
        holder.descriptionView.text = filmItem.Description;
        holder.posterImageView.setImageDrawable(filmItem.Image);
    }

}


class FilmsViewHolder(val view:View):RecyclerView.ViewHolder(view){
    val titleView:TextView;
    val descriptionView:TextView;
    val posterImageView:ImageView;
    init {
        titleView = view.findViewById(R.id.filmTitle)
        descriptionView = view.findViewById(R.id.filmDescription)
        posterImageView = view.findViewById(R.id.posterImage)
    }
}